function test() {
	let obj = {
		searchResults: [
			{ name: "Ritz Carlton", artist: "PR08L3M", album: "Art Brut", id: 1 },
			{
				name: "Dyers Eve",
				artist: "Metallica",
				album: "And Justice For All...",
				id: 2,
			},
			{
				name: "Hacker",
				artist: "Death Grips",
				album: "The Money Store",
				id: 3,
			},
		],
	};
	function removeTrack(track) {
		console.log(track.id);
		return obj.searchResults.filter((e) => {
			return e.id !== 2;
		});
	}
	let traczek = {
		name: "Dyers Eve",
		artist: "Metallica",
		album: "And Justice For All...",
		id: 2,
	};
	removeTrack(traczek);
}

console.log(test());
