/* eslint-disable react/jsx-no-undef */
import React from "react";
import "./App.css";
import SearchResults from "../SearchResults/SearchResults";
import Playlist from "../Playlist/Playlist";
import SearchBar from "../SearchBar/SearchBar";
import Spotify from "../../util/Spotify";

Spotify.getAccessToken();

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			searchResults: [
				{
					name: "Ritz Carlton",
					artist: "PR08L3M",
					album: "Art Brut",
					id: 20,
				},
				{
					name: "Dyers Eve",
					artist: "Metallica",
					album: "And Justice For All...",
					id: 13,
				},
				{
					name: "Hacker",
					artist: "Death Grips",
					album: "The Money Store",
					id: 29,
				},
			],
			playlistName: "moja playlista na spotify",
			playlistTracks: [
				{
					name: "Tiramisu",
					artist: "PR08L3M",
					album: "C30-C39 EP",
					id: 2,
					uri: 
"spotify:track:4VC1xznSLaUJuNWG5fUhmf",
				},
				{
					name: "FALA",
					artist: "Guzior, Oskar83",
					album: "Pleśń",
					id: 13,
					uri: 
"spotify:track:191yHIyhUqBpU2VYNFVbID",
				},
				{
					name: "Avalanche",
					artist: "Nick Cave & The Bad 
Seeds",
					album: "From Her To Eternity",
					id: 17,
					uri: 
"spotify:track:6gq5GDLoyLC9GUkB8Ob78F",
				},
			],
		};
		this.addTrack = this.addTrack.bind(this);
		this.removeTrack = this.removeTrack.bind(this);
		this.updatePlaylistName = 
this.updatePlaylistName.bind(this);
		this.savePlaylist = this.savePlaylist.bind(this);
		this.search = this.search.bind(this);
	}
	addTrack(track) {
		function getRandomInt(max) {
			return Math.floor(Math.random() * max);
		}
		if 
(!Object.keys(this.state.playlistTracks).includes(track.id)) {
			this.setState({
				playlistTracks: [
					...this.state.playlistTracks,
					{
						name: track.name,
						artist: track.artist,
						album: track.album,
						id: getRandomInt(50),
					},
				],
			});
		}
	}
	removeTrack(track) {
		let newState = this.state.playlistTracks.filter((e) => {
			return e.id !== track.id;
		});
		this.setState({ playlistTracks: newState });
	}
	updatePlaylistName(name) {
		this.setState({ playlistName: name });
	}
	savePlaylist() {
		let trackURIs = this.state.playlistTracks.map((track) => 
track.uri);
		Spotify.savePlaylist(this.state.playlistName, trackURIs);
		this.setState({ playlistName: "", playlistTracks: [] });
	}
	search(term) {
		Spotify.search(term).then((searchResults) =>
			this.setState({
				searchResults: searchResults,
			})
		);
	}
	render() {
		return (
			<div>
				<h1>
					Ja<span 
className="highlight">mmm</span>ing
				</h1>
				<div className="App">
					<SearchBar onSearch={this.search} 
/>
					<div className="App-playlist">
						<SearchResults
							
onAdd={this.addTrack}
							
searchResults={this.state.searchResults}
						/>
						<Playlist
							
playlistName={this.state.playlistName}
							
playlistTracks={this.state.playlistTracks}
							
onRemove={this.removeTrack}
							
onNameChange={this.updatePlaylistName}
							
onSave={this.savePlaylist}
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default App;

