//`https://accounts.spotify.com/authorize?client_id=${CLIENT_ID}&response_type=token&scope=playlist-modify-public&redirect_uri=${REDIRECT_URI}`;
const CLIENT_ID = "668beabefb114fecaaf52683bc4e87e9";
const REDIRECT_URI = "http://localhost:3000/";
const spotifyUrl = `https://accounts.spotify.com/authorize?client_id=${CLIENT_ID}&response_type=token&scope=playlist-modify-public&redirect_uri=${REDIRECT_URI}`;
let accessToken = undefined;
let expiryTime = undefined;

const Spotify = {
	getAccessToken() {
		if (accessToken) {
			return accessToken;
		}
		const urlAccessToken = window.location.href.match(/access_token=([^&]*)/);
		const urlExpiresIn = window.location.href.match(/expires_in=([^&]*)/);
		if (urlAccessToken && urlExpiresIn) {
			accessToken = urlAccessToken[1];
			expiryTime = urlExpiresIn[1];
			window.setTimeout(() => (accessToken = ""), expiryTime * 1000);
			window.history.pushState("Access Token", null, "/");
		} else {
			window.location = spotifyUrl;
		}
	},
	search(term) {
		console.log(`ACCESS TOKEN: ${accessToken}`);
		return fetch(`https://api.spotify.com/v1/search?type=track&q=${term}`, {
			headers: {
				Authorization: `Bearer ${accessToken}`,
			},
		})
			.then((response) => response.json())
			.then((responseJson) => {
				console.log(responseJson);
				return Object.keys(responseJson).length === 0
					? []
					: responseJson.tracks.items.map((track) => {
							return {
								id: track.id,
								name: track.name,
								artist: track.artists[0].name,
								album: track.album.name,
								uri: track.uri,
							};
					  });
			});
	},
	savePlaylist(playlistName, trackURIs) {
		console.log("it do be running plz");
		if (playlistName.length === 0 && trackURIs.length === 0) {
			return;
		}
		let headers = {
			Authorization: `Bearer ${accessToken}`,
		};
		let id = undefined;
		let playlistID = undefined;
		const savePlaylistRequest = async () => {
			await fetch("https://api.spotify.com/v1/me", { headers: headers })
				.then((response) => {
					return response.ok ? response.json() : console.log(response.status);
				})
				.then((responseJson) => {
					id = responseJson.id;
					return id;
				})
				.then((id) =>
					fetch(`https://api.spotify.com/v1/users/${id}/playlists`, {
						method: "POST",
						headers: headers,
						body: JSON.stringify({
							name: playlistName,
							public: true,
							collaborative: false,
							description:
								"CREATED WITH JAMMING APP USING SPOTIFY API CALLS :))))",
						}),
					})
						.then((response) => {
							return response.ok
								? response.json()
								: console.log(response.status);
						})
						.then((responseJson) => {
							playlistID = responseJson.id;
							console.log(playlistID);
							return playlistID;
						})
						.then((playlistIDres) => {
							fetch(
								`https://api.spotify.com/v1/playlists/${playlistIDres}/tracks`,
								{
									method: "post",
									headers: headers,
									body: JSON.stringify({
										uris: trackURIs,
									}),
								}
							);
						})
				);
		};
		savePlaylistRequest();
		console.log("it do be running");
	},
};

export default Spotify;
